﻿using System;
using System.Collections.Generic;

namespace API.Models
{
    public partial class PermissionClone
    {
        public int PermissionId { get; set; }
        public int PageId { get; set; }
        public string FunctionId { get; set; }
    }
}
