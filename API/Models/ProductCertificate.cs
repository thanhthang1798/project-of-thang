﻿using System;
using System.Collections.Generic;

namespace API.Models
{
    public partial class ProductCertificate
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string CertificateNumber { get; set; }
        public DateTimeOffset? CertificateDate { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsActive { get; set; }
        public string SearchString { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public int? SortOrder { get; set; }
    }
}
