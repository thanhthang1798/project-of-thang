﻿using System;
using System.Collections.Generic;

namespace API.Models
{
    public partial class SupplierContractShipmentPlannedDetail
    {
        public int Id { get; set; }
        public int SupplierContractShipmentPlannedId { get; set; }
        public int? ProductId { get; set; }
        public double? Quantity { get; set; }
        public DateTimeOffset? ExpectedEtd { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public int? SortOrder { get; set; }
        public string ExpectedEtdNote { get; set; }
    }
}
