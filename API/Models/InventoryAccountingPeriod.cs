﻿using System;
using System.Collections.Generic;

namespace API.Models
{
    public partial class InventoryAccountingPeriod
    {
        public int InventoryAccountingPeriodId { get; set; }
        public string InventoryAccountingPeriodName { get; set; }
        public string InventoryAccountingPeriodCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
    }
}
