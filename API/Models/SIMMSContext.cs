﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace API.Models
{
    public partial class SIMMSContext : DbContext
    {
        public SIMMSContext(DbContextOptions<SIMMSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AttachFile> AttachFile { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Currency> Currency { get; set; }
        public virtual DbSet<CustomsDeclaration> CustomsDeclaration { get; set; }
        public virtual DbSet<CustomsDeclarationContainer> CustomsDeclarationContainer { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<ExchangeRate> ExchangeRate { get; set; }
        public virtual DbSet<Fiscal> Fiscal { get; set; }
        public virtual DbSet<Function> Function { get; set; }
        public virtual DbSet<FunctionClone> FunctionClone { get; set; }
        public virtual DbSet<Harbor> Harbor { get; set; }
        public virtual DbSet<InternalOrder> InternalOrder { get; set; }
        public virtual DbSet<InternalOrderDetail> InternalOrderDetail { get; set; }
        public virtual DbSet<InventoryAccountingBatchReport> InventoryAccountingBatchReport { get; set; }
        public virtual DbSet<InventoryAccountingPeriod> InventoryAccountingPeriod { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemGroup> ItemGroup { get; set; }
        public virtual DbSet<ItemgroupItems> ItemgroupItems { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Module> Module { get; set; }
        public virtual DbSet<NotificationSend> NotificationSend { get; set; }
        public virtual DbSet<NotificationType> NotificationType { get; set; }
        public virtual DbSet<NotificationTypeRole> NotificationTypeRole { get; set; }
        public virtual DbSet<NotificationTypeUser> NotificationTypeUser { get; set; }
        public virtual DbSet<OptionSystem> OptionSystem { get; set; }
        public virtual DbSet<PackageUnit> PackageUnit { get; set; }
        public virtual DbSet<Page> Page { get; set; }
        public virtual DbSet<PageClone> PageClone { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<PermissionClone> PermissionClone { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductBk1703> ProductBk1703 { get; set; }
        public virtual DbSet<ProductCertificate> ProductCertificate { get; set; }
        public virtual DbSet<ProductImportTax> ProductImportTax { get; set; }
        public virtual DbSet<ProductList> ProductList { get; set; }
        public virtual DbSet<ProductListforErp> ProductListforErp { get; set; }
        public virtual DbSet<ProductPackageUnit> ProductPackageUnit { get; set; }
        public virtual DbSet<ProductVat> ProductVat { get; set; }
        public virtual DbSet<RefNo> RefNo { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RolePermission> RolePermission { get; set; }
        public virtual DbSet<RolePermissionClone> RolePermissionClone { get; set; }
        public virtual DbSet<ShipmentActualDetailBatch> ShipmentActualDetailBatch { get; set; }
        public virtual DbSet<ShipmentFollowUp> ShipmentFollowUp { get; set; }
        public virtual DbSet<ShipmentTotal> ShipmentTotal { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<Store> Store { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<SupplierContract> SupplierContract { get; set; }
        public virtual DbSet<SupplierContractLog> SupplierContractLog { get; set; }
        public virtual DbSet<SupplierContractProduct> SupplierContractProduct { get; set; }
        public virtual DbSet<SupplierContractQuantityCustomsDeclarationTotal> SupplierContractQuantityCustomsDeclarationTotal { get; set; }
        public virtual DbSet<SupplierContractQuantityRemaining> SupplierContractQuantityRemaining { get; set; }
        public virtual DbSet<SupplierContractShipmentActual> SupplierContractShipmentActual { get; set; }
        public virtual DbSet<SupplierContractShipmentActualDetail> SupplierContractShipmentActualDetail { get; set; }
        public virtual DbSet<SupplierContractShipmentJoin> SupplierContractShipmentJoin { get; set; }
        public virtual DbSet<SupplierContractShipmentPlanned> SupplierContractShipmentPlanned { get; set; }
        public virtual DbSet<SupplierContractShipmentPlannedDetail> SupplierContractShipmentPlannedDetail { get; set; }
        public virtual DbSet<SupplierContractStatusHistory> SupplierContractStatusHistory { get; set; }
        public virtual DbSet<SupplierContractType> SupplierContractType { get; set; }
        public virtual DbSet<SysConfig> SysConfig { get; set; }
        public virtual DbSet<TokenRefresh> TokenRefresh { get; set; }
        public virtual DbSet<TonKho0403> TonKho0403 { get; set; }
        public virtual DbSet<Unit> Unit { get; set; }
        public virtual DbSet<UnitExchange> UnitExchange { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserDepartment> UserDepartment { get; set; }
        public virtual DbSet<UserGroupItem> UserGroupItem { get; set; }
        public virtual DbSet<UserItem> UserItem { get; set; }
        public virtual DbSet<UserPermission> UserPermission { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<UserRoleClone> UserRoleClone { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttachFile>(entity =>
            {
                entity.HasKey(e => e.AttachId);

                entity.Property(e => e.AttachId).HasColumnName("AttachID");

                entity.Property(e => e.Desctiption).HasMaxLength(500);

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileNameOriginal).HasMaxLength(250);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.FileTitle).HasMaxLength(250);

                entity.Property(e => e.KeyValue)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OptionName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("Country", "list");

                entity.Property(e => e.CountryId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("Currency", "list");

                entity.Property(e => e.CurrencyId)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasComment("Người tạo");

                entity.Property(e => e.CreatedDate).HasComment("Ngày tạo");

                entity.Property(e => e.CurrencyName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsMainCurrency).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifyBy).HasComment("Người update");

                entity.Property(e => e.ModifyDate).HasComment("Ngày update");

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.SearchString).HasMaxLength(2000);
            });

            modelBuilder.Entity<CustomsDeclaration>(entity =>
            {
                entity.HasComment("Thong tin phieu XNK");

                entity.Property(e => e.Bldate).HasColumnName("BLDate");

                entity.Property(e => e.Blnumber)
                    .HasColumnName("BLNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BoatFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi hang tau");

                entity.Property(e => e.CertificateDate).HasComment("Ngay chung thu");

                entity.Property(e => e.CertificateNumber)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("So chung thu");

                entity.Property(e => e.CoAdjust)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CoNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoThirdNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContainerNumber)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("So container");

                entity.Property(e => e.ContainerType).HasComment("Loai container");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Currency)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("Tien te");

                entity.Property(e => e.CustomsClearanceDate).HasComment("Ngay thong quan");

                entity.Property(e => e.CustomsFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi hai quan");

                entity.Property(e => e.CustomsName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeclarationCode)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("Ma phieu XNKL");

                entity.Property(e => e.DeclarationDate).HasComment("Ngay tao phieu");

                entity.Property(e => e.ExchangeRate).HasComment("Ty gia");

                entity.Property(e => e.FreightDownFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi nang ha");

                entity.Property(e => e.HarborFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi cang");

                entity.Property(e => e.ImportCerNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImportTaxTotal)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Tong thue nhap khau");

                entity.Property(e => e.InputDate).HasComment("Ngay nhap kho");

                entity.Property(e => e.InsuranceFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi bao hiem");

                entity.Property(e => e.InventoryId).HasComment("Kho");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.OtherFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi khac");

                entity.Property(e => e.ReceivedDate).HasComment("Ngay nhan thong bao");

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.ShipmentBy)
                    .HasMaxLength(250)
                    .HasComment("Dang van chuyen");

                entity.Property(e => e.SpecializedFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi chuyen nganh");

                entity.Property(e => e.TkhqDate).HasComment("Ngày lap to khai hai quan - TKHQ");

                entity.Property(e => e.TkhqNo)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasComment("So to khai hai quan- TKHQ");

                entity.Property(e => e.Total)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Tong tien lanh hang");

                entity.Property(e => e.TransportFee)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Phi van chuyen");

                entity.Property(e => e.VatTotal)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Tong VAT");
            });

            modelBuilder.Entity<CustomsDeclarationContainer>(entity =>
            {
                entity.Property(e => e.ContainerCode).HasColumnType("ntext");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("Department", "system");

                entity.Property(e => e.DepartmentName).HasMaxLength(250);

                entity.Property(e => e.Icon).HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.MailGroup).HasMaxLength(250);

                entity.Property(e => e.Notes).HasColumnType("ntext");
            });

            modelBuilder.Entity<ExchangeRate>(entity =>
            {
                entity.ToTable("ExchangeRate", "list");

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasComment("Loại tiền");

                entity.Property(e => e.ExchangeDate).HasComment("Ngày xét tỷ giá");

                entity.Property(e => e.ExchangeRating)
                    .HasColumnType("numeric(18, 0)")
                    .HasComment("Tỷ giá so với loại tiền chính");

                entity.Property(e => e.Notes).HasMaxLength(250);
            });

            modelBuilder.Entity<Fiscal>(entity =>
            {
                entity.Property(e => e.ClosePriceDate).HasColumnType("date");

                entity.Property(e => e.FiscalName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.Notes).HasMaxLength(4000);

                entity.Property(e => e.ToDate).HasColumnType("date");
            });

            modelBuilder.Entity<Function>(entity =>
            {
                entity.ToTable("Function", "security");

                entity.Property(e => e.FunctionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsSpecial).HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes).HasMaxLength(500);
            });

            modelBuilder.Entity<FunctionClone>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Function_Clone", "security");

                entity.Property(e => e.FunctionId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsSpecial).HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes).HasMaxLength(500);
            });

            modelBuilder.Entity<Harbor>(entity =>
            {
                entity.ToTable("Harbor", "list");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HarborCode)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.HarborName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InternalOrder>(entity =>
            {
                entity.HasComment("Don dat hang noi bo");

                entity.Property(e => e.ById).HasComment("Sea/Air");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.InternalOrderCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(2000);
            });

            modelBuilder.Entity<InternalOrderDetail>(entity =>
            {
                entity.HasComment("Chi tiet don dat hang noi bo");

                entity.Property(e => e.BookedEta)
                    .HasColumnName("BookedETA")
                    .HasComment("EDT dat hang");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateClearance).HasComment("Ngay thong quan");

                entity.Property(e => e.DateToStore).HasComment("Ngay ve kho");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.QuantityRequest).HasComment("So luong de de");

                entity.Property(e => e.RequestEta)
                    .HasColumnName("RequestETA")
                    .HasComment("Ngay Request ETA");
            });

            modelBuilder.Entity<InventoryAccountingBatchReport>(entity =>
            {
                entity.HasComment("Bao cao ton kho theo lo cua Ke toan");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExpiredDateAccounting).HasColumnType("date");

                entity.Property(e => e.InventoryAccountingPeriodId).HasComment(@"Ky ke toan
   ");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.LotNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NoteAccounting).HasMaxLength(800);

                entity.Property(e => e.ProductionDateAccounting).HasColumnType("date");
            });

            modelBuilder.Entity<InventoryAccountingPeriod>(entity =>
            {
                entity.HasComment("Danh muc Ky ke toan cho ton kho");

                entity.Property(e => e.InventoryAccountingPeriodId).HasComment("Id tu tang");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasComment("Ngay ket thuc");

                entity.Property(e => e.InventoryAccountingPeriodCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComment("Ma ky ke toan. Ex: 01032020");

                entity.Property(e => e.InventoryAccountingPeriodName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasComment(@"Ten ky ke toan
   ");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasComment("Ngay bat dau");
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.ToTable("Item", "das");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.Type).HasMaxLength(250);
            });

            modelBuilder.Entity<ItemGroup>(entity =>
            {
                entity.ToTable("ItemGroup", "das");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Notes).HasColumnType("ntext");

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.Type).HasMaxLength(250);
            });

            modelBuilder.Entity<ItemgroupItems>(entity =>
            {
                entity.ToTable("ItemgroupItems", "das");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Notes).HasColumnType("ntext");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasKey(e => new { e.Code, e.LangId });

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LangId)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Messages)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Notes).HasMaxLength(500);
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.ToTable("Module", "security");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModuleName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Notes).HasMaxLength(500);
            });

            modelBuilder.Entity<NotificationSend>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSent).HasComment("Da gui");

                entity.Property(e => e.SentDate).HasComment("Ngay gui");
            });

            modelBuilder.Entity<NotificationType>(entity =>
            {
                entity.HasComment("Loai thong bao");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.Template)
                    .IsRequired()
                    .HasMaxLength(400);
            });

            modelBuilder.Entity<NotificationTypeRole>(entity =>
            {
                entity.HasComment("Phan quyen gui thong bao cho Role");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString).HasColumnType("text");
            });

            modelBuilder.Entity<NotificationTypeUser>(entity =>
            {
                entity.HasComment("Phan quyen gui thong bao theo User");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString).HasColumnType("text");
            });

            modelBuilder.Entity<OptionSystem>(entity =>
            {
                entity.HasKey(e => e.OptionId);

                entity.Property(e => e.DataType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.OptionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OptionType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<PackageUnit>(entity =>
            {
                entity.ToTable("PackageUnit", "list");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.PackageUnitName).HasMaxLength(100);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.UnitExchangeId).HasComment("Don vi quy doi (Kg: 1, LB: 2,...)");

                entity.Property(e => e.Volume)
                    .HasColumnType("decimal(18, 0)")
                    .HasComment("Khoi luong");
            });

            modelBuilder.Entity<Page>(entity =>
            {
                entity.ToTable("Page", "security");

                entity.Property(e => e.ActionName).HasMaxLength(100);

                entity.Property(e => e.ControllerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FormName).HasMaxLength(100);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsCheckSecurity).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsRunOnStore).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsUserEditableData).HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.PageName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Parameter)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PageClone>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Page_Clone", "security");

                entity.Property(e => e.ActionName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ControllerName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FormName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsCheckSecurity).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsRunOnStore).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsUserEditableData).HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.PageId).ValueGeneratedOnAdd();

                entity.Property(e => e.PageName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Parameter)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.ToTable("Permission", "security");

                entity.Property(e => e.FunctionId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PermissionClone>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Permission_Clone", "security");

                entity.Property(e => e.FunctionId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.ToTable("Position", "system");

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Icon).HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.MailGroup).HasMaxLength(250);

                entity.Property(e => e.PositionName).HasMaxLength(250);

                entity.Property(e => e.PositionShortName).HasMaxLength(250);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product", "list");

                entity.Property(e => e.CorrectName)
                    .HasMaxLength(800)
                    .HasComment("Su dung chinh thuc");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.OriginId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCommerceName)
                    .HasMaxLength(400)
                    .HasComment("Ten cong bo");

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.Remark).HasMaxLength(2000);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("Ma bieu thue ");

                entity.Property(e => e.UnitExchangeId).HasComment("Don vi quy doi: Kg, LB");
            });

            modelBuilder.Entity<ProductBk1703>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Product_BK_1703", "list");

                entity.Property(e => e.CorrectName)
                    .HasMaxLength(800)
                    .HasComment("Su dung chinh thuc");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.OriginId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCommerceName)
                    .HasMaxLength(400)
                    .HasComment("Ten cong bo");

                entity.Property(e => e.ProductId).ValueGeneratedOnAdd();

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.Remark).HasMaxLength(2000);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TaxCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("Ma bieu thue ");

                entity.Property(e => e.UnitExchangeId).HasComment("Don vi quy doi: Kg, LB");
            });

            modelBuilder.Entity<ProductCertificate>(entity =>
            {
                entity.ToTable("ProductCertificate", "list");

                entity.HasComment("Cong bo san pham");

                entity.Property(e => e.CertificateDate).HasComment("ngay cong bo");

                entity.Property(e => e.CertificateNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("so cong bo");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductImportTax>(entity =>
            {
                entity.ToTable("ProductImportTax", "list");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ImportCode)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("HS Code");

                entity.Property(e => e.ImportTax).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDefault).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductList>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ProductList", "temp");

                entity.Property(e => e.Note).HasMaxLength(400);

                entity.Property(e => e.Origin).HasMaxLength(250);

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName).HasMaxLength(400);

                entity.Property(e => e.SuggestedCorrectName).HasMaxLength(800);

                entity.Property(e => e.Supplier).HasMaxLength(400);
            });

            modelBuilder.Entity<ProductListforErp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ProductListforERP");

                entity.Property(e => e.Column1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Column2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CácQuyCáchĐóngGói)
                    .HasColumnName("Các quy cách đóng gói")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DanhSáchTop5ỨngDụngĐiểnHìnhCủaMặtHàngNày)
                    .HasColumnName("Danh sách top 5 ứng dụng điển hình của mặt hàng này")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DòngGiớiThiệuTómTắtVềSp)
                    .HasColumnName("Dòng giới thiệu tóm tắt về sp")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ncc)
                    .HasColumnName("NCC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NguồnGốcXuấtXứ)
                    .HasColumnName("Nguồn gốc xuất xứ")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Origin)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName)
                    .HasColumnName("Product_name")
                    .HasMaxLength(100);

                entity.Property(e => e.Stt).HasColumnName("STT");

                entity.Property(e => e.SuggestedCorrectName)
                    .HasColumnName("Suggested Correct name")
                    .HasMaxLength(150);

                entity.Property(e => e.ThuộcNhómHàng)
                    .HasColumnName("Thuộc nhóm hàng?")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TênThươngMạiSẽHiệnTrênWebsite)
                    .HasColumnName("Tên thương mại (sẽ hiện trên website)")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WebsitesThamKhảoNếuCóVềMặtHàngNày)
                    .HasColumnName("Websites tham khảo, nếu có, về mặt hàng này")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductPackageUnit>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ProductId, e.PackageUnitId })
                    .HasName("PK_PRODUCTPACKAGEUNIT");

                entity.ToTable("ProductPackageUnit", "list");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString).HasColumnType("text");
            });

            modelBuilder.Entity<ProductVat>(entity =>
            {
                entity.ToTable("ProductVAT", "list");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDefault).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Vat)
                    .HasColumnName("VAT")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<RefNo>(entity =>
            {
                entity.HasKey(e => e.RowId);

                entity.Property(e => e.RowId).ValueGeneratedNever();

                entity.Property(e => e.FormateString)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.RRefType).HasColumnName("rRefType");

                entity.Property(e => e.RefType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SequenceName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.SqlQueryRefNo)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.SqlQueryRefNoSql)
                    .HasColumnName("SqlQueryRefNoSQL")
                    .HasMaxLength(2000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role", "security");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(4000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RolePermission>(entity =>
            {
                entity.ToTable("RolePermission", "security");
            });

            modelBuilder.Entity<RolePermissionClone>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("RolePermission_Clone", "security");

                entity.Property(e => e.RolePermissionId).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ShipmentActualDetailBatch>(entity =>
            {
                entity.HasComment("Chi tiet shipment theo lo");

                entity.Property(e => e.Id).HasComment("So luong theo lo");

                entity.Property(e => e.BeginPeriodAccounting)
                    .HasColumnType("decimal(18, 2)")
                    .HasComment("Dau ky");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndPeriodAccounting)
                    .HasColumnType("decimal(18, 2)")
                    .HasComment("Cuoi ky");

                entity.Property(e => e.ExpiredDate).HasColumnType("decimal(18, 1)");

                entity.Property(e => e.ExpiredDateAccounting).HasComment("Ngay het han tu du lieu cua Ke toan");

                entity.Property(e => e.ExportQuantityAccounting)
                    .HasColumnType("decimal(18, 2)")
                    .HasComment("Xuat kho");

                entity.Property(e => e.ImportQuantityAccounting)
                    .HasColumnType("decimal(18, 2)")
                    .HasComment("Nhap kho");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsImportFromAccounting)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Danh dau du lieu duoc import tu Ke toan");

                entity.Property(e => e.LotNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NoteAccounting)
                    .HasMaxLength(800)
                    .HasComment("Ghi chu cua ke toan");

                entity.Property(e => e.ProductId).HasComment("Product cua ke toan");

                entity.Property(e => e.ProductionDateAccounting).HasComment("Ngay san xuat tu du lieu cua Ke toan");

                entity.Property(e => e.QuantityToStore)
                    .HasColumnType("decimal(18, 2)")
                    .HasComment("So luong ve kho thuc te");

                entity.Property(e => e.StoreIdAccounting).HasComment("Hang ve kho nao theo du lieu ke toan");
            });

            modelBuilder.Entity<ShipmentFollowUp>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ShipmentFollowUp", "temp");

                entity.Property(e => e.BLNo)
                    .HasColumnName("B/L No")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractSupplier)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Customer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Eta)
                    .HasColumnName("ETA")
                    .HasColumnType("date");

                entity.Property(e => e.Etd)
                    .HasColumnName("ETD")
                    .HasColumnType("date");

                entity.Property(e => e.InsFeeDoortodoor).HasColumnName("InsFee(doortodoor)");

                entity.Property(e => e.InsFeePorttowarehouse).HasColumnName("InsFee(porttowarehouse)");

                entity.Property(e => e.InvNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Invdate).HasColumnType("date");

                entity.Property(e => e.IsSendAccount).HasMaxLength(50);

                entity.Property(e => e.IsSendInvoice).HasMaxLength(100);

                entity.Property(e => e.Item).HasMaxLength(100);

                entity.Property(e => e.Note).HasMaxLength(50);

                entity.Property(e => e.Poankhai)
                    .HasColumnName("POANKHAI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Supplier)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValueUsd).HasColumnName("Value(USD)");
            });

            modelBuilder.Entity<ShipmentTotal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ShipmentTotal", "temp");

                entity.Property(e => e.ArriveDate).HasColumnType("date");

                entity.Property(e => e.CertDate).HasColumnType("date");

                entity.Property(e => e.CertNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CobenThu3)
                    .HasColumnName("COBenThu3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Codate)
                    .HasColumnName("CODate")
                    .HasColumnType("date");

                entity.Property(e => e.ContractDate).HasColumnType("date");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Conumber)
                    .HasColumnName("CONumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cpbhiem).HasColumnName("CPBHiem");

                entity.Property(e => e.Cpvchuyen).HasColumnName("CPVChuyen");

                entity.Property(e => e.Curency)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomMaDate).HasColumnType("date");

                entity.Property(e => e.CustomMaNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoaDon)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HqdieuChinh).HasColumnName("HQDieuChinh");

                entity.Property(e => e.ImportCerNo)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.InvoiceNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaBieuThue)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaNk)
                    .HasColumnName("MaNK")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName)
                    .HasColumnName("Product_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.QuocGia)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Remark).HasMaxLength(250);

                entity.Property(e => e.TbhdenDate)
                    .HasColumnName("TBHDenDate")
                    .HasColumnType("date");

                entity.Property(e => e.TenCchq)
                    .HasColumnName("TenCCHQ")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ThueNk).HasColumnName("ThueNK");

                entity.Property(e => e.ThueVat).HasColumnName("ThueVAT");

                entity.Property(e => e.TienThueNk).HasColumnName("TienThueNK");

                entity.Property(e => e.TienThueVat).HasColumnName("TienThueVAT");

                entity.Property(e => e.TongBh).HasColumnName("TongBH");

                entity.Property(e => e.TongNte)
                    .HasColumnName("TongNTe")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TongThueNk)
                    .HasColumnName("TongThueNK")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TongTienThue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TongTienVnd)
                    .HasColumnName("TongTienVND")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TongVat)
                    .HasColumnName("TongVAT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TongVc).HasColumnName("TongVC");

                entity.Property(e => e.TriGiaKbNt)
                    .HasColumnName("TriGiaKB_NT")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TriGiaKbVnd)
                    .HasColumnName("TriGiaKB_VND")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("Status", "system");

                entity.HasComment("Trang thai quy trinh cho toan bo he thong");

                entity.Property(e => e.Description).HasMaxLength(800);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.StatusName).HasMaxLength(200);
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.ToTable("Store", "list");

                entity.Property(e => e.Address).HasColumnType("ntext");

                entity.Property(e => e.StoreName).HasMaxLength(250);
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier", "list");

                entity.Property(e => e.AbbreviatedName).HasMaxLength(255);

                entity.Property(e => e.CountryId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.PaymentTerm).HasColumnType("ntext");

                entity.Property(e => e.Remark).HasMaxLength(2000);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierAddress).HasMaxLength(2000);

                entity.Property(e => e.SupplierCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierName)
                    .IsRequired()
                    .HasMaxLength(400);
            });

            modelBuilder.Entity<SupplierContract>(entity =>
            {
                entity.Property(e => e.ContractDate).HasComment("Ngay hop dong ");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ExpectedEtd).HasColumnName("ExpectedETD");

                entity.Property(e => e.ExpectedNote).HasMaxLength(2000);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasMaxLength(2000);

                entity.Property(e => e.PaymentTerm).HasColumnType("ntext");

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.StatusId).HasComment("Trang thai hop dong");

                entity.Property(e => e.SupplierName).HasMaxLength(250);

                entity.Property(e => e.SupplierPo)
                    .HasColumnName("SupplierPO")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ValidFromDate).HasComment("Hieu luc tu ngay");

                entity.Property(e => e.ValidToDate).HasComment("Hieu luc den ngay");
            });

            modelBuilder.Entity<SupplierContractLog>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnType("ntext");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<SupplierContractProduct>(entity =>
            {
                entity.Property(e => e.Currency).HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.OriginId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName).HasMaxLength(250);
            });

            modelBuilder.Entity<SupplierContractQuantityCustomsDeclarationTotal>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("SupplierContractQuantityCustomsDeclarationTotal");

                entity.Property(e => e.Blnumber)
                    .HasColumnName("BLNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNumber).HasMaxLength(250);

                entity.Property(e => e.Vattotal).HasColumnName("VATTotal");

                entity.Property(e => e.Vndtotal).HasColumnName("VNDTotal");
            });

            modelBuilder.Entity<SupplierContractQuantityRemaining>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("SupplierContractQuantityRemaining");

                entity.HasComment("Quota cua san pham theo hop dong");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CountryName).HasMaxLength(250);

                entity.Property(e => e.Currency).HasMaxLength(250);

                entity.Property(e => e.PackageUnitName).HasMaxLength(100);

                entity.Property(e => e.ProductByContractId)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName).HasMaxLength(400);

                entity.Property(e => e.TotalShipmentQuantity).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<SupplierContractShipmentActual>(entity =>
            {
                entity.Property(e => e.BaseFee).HasColumnType("decimal(18, 5)");

                entity.Property(e => e.BookedEta).HasColumnName("BookedETA");

                entity.Property(e => e.BookedEtd).HasColumnName("BookedETD");

                entity.Property(e => e.Currency).HasMaxLength(250);

                entity.Property(e => e.CustomsDeclarationId).HasComment("Ma phieu XNK");

                entity.Property(e => e.HarborTo).HasComment("Cang den (Luu theo ID cua cang)");

                entity.Property(e => e.InsurranceFee).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.InsurranceTypeId).HasComment("Loai bao hiem");

                entity.Property(e => e.InvoiceNumber).HasMaxLength(250);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsFullDocument)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Da nhan day du chung tu");

                entity.Property(e => e.IsSend).HasComment("Gui Invoice cho ke toan ?");

                entity.Property(e => e.JointShipmentId).HasComment("Co ghep hang tu hop dong khac ?");

                entity.Property(e => e.Note).HasColumnType("ntext");

                entity.Property(e => e.RequestedEta).HasColumnName("RequestedETA");

                entity.Property(e => e.RequestedEtd)
                    .HasColumnName("RequestedETD")
                    .HasComment("Ngay load hang");

                entity.Property(e => e.SearchString).HasColumnType("text");

                entity.Property(e => e.Sosupplier)
                    .HasColumnName("SOSupplier")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StoreTo).HasMaxLength(250);

                entity.Property(e => e.SupplierOrderId).HasMaxLength(250);

                entity.Property(e => e.WaybillCode).HasMaxLength(250);
            });

            modelBuilder.Entity<SupplierContractShipmentActualDetail>(entity =>
            {
                entity.Property(e => e.BatchNote).HasColumnType("ntext");

                entity.Property(e => e.ExpiredDate).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ImportTax)
                    .HasColumnType("decimal(10, 2)")
                    .HasComment("Thu? NK");

                entity.Property(e => e.ImportTaxCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("Mã bi?u thu?");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsJoin)
                    .HasDefaultValueSql("((0))")
                    .HasComment("Danh dau san pham duoc ghep tu hop dong khac");

                entity.Property(e => e.LotNumber).HasMaxLength(250);

                entity.Property(e => e.MadeIn)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PackageUnitId).HasComment("Quy cach thong thuong");

                entity.Property(e => e.PackageUnitPalletId).HasComment("Quy cach theo Pallet");

                entity.Property(e => e.ProductName).HasMaxLength(250);

                entity.Property(e => e.Vat)
                    .HasColumnName("VAT")
                    .HasColumnType("decimal(10, 2)")
                    .HasComment("Thu? VAT");
            });

            modelBuilder.Entity<SupplierContractShipmentJoin>(entity =>
            {
                entity.Property(e => e.BlCode).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Note).HasColumnType("ntext");
            });

            modelBuilder.Entity<SupplierContractShipmentPlanned>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShipmentName).HasMaxLength(250);
            });

            modelBuilder.Entity<SupplierContractShipmentPlannedDetail>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExpectedEtdNote).HasMaxLength(800);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<SupplierContractStatusHistory>(entity =>
            {
                entity.Property(e => e.Notes).HasColumnType("ntext");

                entity.Property(e => e.SearchString).HasColumnType("text");
            });

            modelBuilder.Entity<SupplierContractType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.SearchString).HasColumnType("text");
            });

            modelBuilder.Entity<SysConfig>(entity =>
            {
                entity.ToTable("SysConfig", "system");

                entity.Property(e => e.ConfigCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConfigValue).HasMaxLength(2000);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasMaxLength(800);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.SearchString).HasColumnType("text");
            });

            modelBuilder.Entity<TokenRefresh>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Refreshtoken)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Revoked).HasDefaultValueSql("((0))");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TonKho0403>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TonKho0403", "temp");

                entity.Property(e => e.Dvt)
                    .HasColumnName("DVT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaHang)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NgayHetHan).HasColumnType("date");

                entity.Property(e => e.NgaySanXuat).HasColumnType("date");

                entity.Property(e => e.SoLo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TenHang).HasMaxLength(400);
            });

            modelBuilder.Entity<Unit>(entity =>
            {
                entity.ToTable("Unit", "list");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnitName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UnitExchange>(entity =>
            {
                entity.ToTable("UnitExchange", "list");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.UnitExchangeName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User", "system");

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.AdminCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Avatar)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSystem).HasDefaultValueSql("((0))");

                entity.Property(e => e.PageDefault)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordExpire).HasColumnType("date");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SearchString)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.SecondPassword)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SignatureImage)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.SystemLanguage)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UserCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserDepartment>(entity =>
            {
                entity.ToTable("UserDepartment", "system");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<UserGroupItem>(entity =>
            {
                entity.ToTable("UserGroupItem", "das");

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes).HasColumnType("ntext");
            });

            modelBuilder.Entity<UserItem>(entity =>
            {
                entity.ToTable("UserItem", "das");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Notes).HasColumnType("ntext");
            });

            modelBuilder.Entity<UserPermission>(entity =>
            {
                entity.ToTable("UserPermission", "security");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("UserRole", "security");
            });

            modelBuilder.Entity<UserRoleClone>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("UserRole_Clone", "security");

                entity.Property(e => e.UserRoleId).ValueGeneratedOnAdd();
            });

            modelBuilder.HasSequence("CustomsDeclarationSEQ");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
