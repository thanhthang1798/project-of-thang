﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Interfaces
{
    public interface ITokenRefresh
    {
        TokenRefresh GetData(string Key);

        bool Save(TokenRefresh tokenRefresh, bool isNew);
    }
}
